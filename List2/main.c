#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define FREE(T) do { free (T); T = NULL; } while (0)

typedef struct listNode node;
typedef struct list List;

struct listNode{
    int value;
    node* next;
    node* prev;
};

struct list{
    node* head;
    node* tail;
    int size;
};

List* create(){
    List* list = (List*) malloc(sizeof(List*));
    list->head = NULL;
    list->tail = NULL;
    list->size = 0;
    return list;
}

void enqueueHead(List* list, int value){
    node* element = (node*) malloc(sizeof(node*));
    element->value = value;

    if(list->size == 0){
        list->head = element;
        list->tail = element;
        element->next = element;
        element->prev = element;
    }
    else{
        element->next = list->head;
        list->tail->next = element;
        element->prev = list->tail;
        list->head->prev = element;
        list->head = element;

    }

    ++list->size;
}

void enqueueTail(List* list, int value){
    node* element = (node*) malloc(sizeof(node*));
    element->value = value;

    if(list->size == 0){
        list->head = element;
        list->tail = element;
        element->next = element;
        element->prev = element;
    }
    else{
        element->next = list->head;
        list->tail->next = element;
        element->prev = list->tail;
        list->head->prev = element;
        list->tail = element;
    }

    ++list->size;
}

int dequeueHead(List* list){
    if(list->size > 1){
        int value = list->head->value;

        list->head->next->prev = list->tail;
        list->head = list->head->next;
        FREE(list->tail->next);
        list->tail->next = list->head;
        --list->size;
        return value;
    }else if(list->size == 1){
        int value = list->head->value;
        FREE(list->head);
        --list->size;
        return value;
    }
    else{
        printf("\nDequeue error: queue empty\n");
        return -1;
    }
}

int dequeueTail(List* list){
    if(list->size > 1){
        int value = list->tail->value;

        list->tail = list->tail->prev;
        FREE(list->tail->next);
        list->tail->next = list->head;
        list->head->prev = list->tail;
        --list->size;
        return value;
    }
    else if(list->size == 1){
        int value = list->head->value;
        FREE(list->head);
        --list->size;
        return value;
    }
    else{
        printf("\nDequeue error: queue empty\n");
        return -1;
    }
}

int getValue(List* list, int position){
    if(position > list->size || position < 1){
        printf("wrong position indicator\n");
        return -1;
    }
    int i;
    node* element;
    if(position <= list->size/2){
        //printf("forward\n");
        element = list->head;
        for(i = 1; i < position; i++){
            element = element->next;
        }
    }
    else{
        //printf("backward\n");
        element = list->tail;
        for(i = 1; i < (list->size - position) + 1; i++){
            element = element->prev;
        }
    }
    return element->value;
}

void empty(List* list){
    node* element = list->head;
    while(list->size > 2){
        FREE(element->prev);
        element = element->next;
        --list->size;
    }
    FREE(element->prev);
    FREE(element);
    list->size -= 2;
}

void destroy(List* list){
    if(list->size > 0){
        empty(list);
    }
    FREE(list);
    list = NULL;
}

int search(List* list, int value){
    int position = 1;
    node* element = list->head;

    do{
        if(value == element->value){
            printf("searching success. Value %d found on position: %d\n",value, position);
            break;
        }
        element = element->next;
        ++position;
    }while(element != list->head);

    if(position == list->size + 1){
        printf("Searching failed. Returning value 0\n");
        return 0;
    }
    return position;

}

void merge(List* list1, List* list2){
    //List* merged;
    printf("merging\n");
    list1->tail->next = list2->head;
    list2->head->prev = list1->tail;
    list1->tail = list2->tail;
    list2->tail->next = list1->head;
    list1->size += list2->size;
    FREE(list2);
}

void randomAccess(List* list){
    int i = 0;
    double avg = 0;
    clock_t start_t, end_t;
    for(i = 1; i<=50; i++){
        start_t = clock();
        getValue(list, rand()%1000);
        //printf("value: %d\n", getValue(list, rand()%1000));
        end_t = clock();
        //printf("time: %ld\n", (end_t - start_t));
        avg += (end_t - start_t);
    }
    printf("avg for random: %lf\n", avg/50);
}
void constAccess(List* list){
    int i = 0;
    int j=800;
    double avg = 0;
    clock_t start_t, end_t;
    for(i = 1; i<50; i++){
        //j = i*20-1;
        start_t = clock();
        getValue(list, j);
        //printf("value: %d\n", getValue(list, rand()%1000));
        end_t = clock();
        //printf("time: %ld\n", (end_t - start_t));
        avg += (end_t - start_t);
    }
    printf("avg for constant position: %lf\n", avg/50);
}

void accessTest(){
    int i;
    int seed = time(NULL);
    srand(seed);
    List* list = create();
    for(i = 0; i<1000; i++){
        enqueueHead(list, rand());
        //printf("%d\n", list->head->value);
    }

    for(i=0; i<10; ++i){
        randomAccess(list);
    }
    for(i=0; i<10; ++i){
        constAccess(list);
    }

}


void greatTest(List* list){
    int i;
    int j;
    for(i = 0; i<1000000; i++){
        enqueueHead(list, i);
        enqueueTail(list, i);
    }
    for(i = 1; i<=1000000; i++){
        j = dequeueHead(list);
        j = dequeueTail(list);
    }
}

int main(void)
{
    accessTest();
/*
    List* list = create();

/*
    enqueueHead(list, 10);
    enqueueHead(list, 20);
    enqueueHead(list, 30);
    enqueueHead(list, 50);
    enqueueHead(list, 60);
    enqueueTail(list, 70);
    enqueueTail(list, 80);
    enqueueTail(list, 90);
    enqueueHead(list, 40);
    int i;
    node* element = list->head;
    for(i = 0; i < list->size; i++){
        printf("nr %d = %d \n", i+1, element->value);
        element = element->next;
    }

    printf("search: %d\n", search(list, 100));

    printf("dequeueTail: %d\n", dequeueTail(list));
    printf("dequeueHead: %d\n", dequeueHead(list));
    printf("size: %d\n", list->size);


    enqueueHead(list, 50);
    enqueueHead(list, 60);
    printf("size: %d\n", list->size);
    element = list->head;
    for(i = 0; i < list->size; i++){
        printf("nr %d = %d == %d \n", i+1, element->value, getValue(list, i+1));
        element = element->next;
    }
*//*
    clock_t start_t, end_t;
    start_t = clock();
    int i = 0;
    for(i = 0; i<20; i++){
        start_t = clock();
        greatTest(list);
        end_t = clock();
        printf("%ld\n", end_t - start_t);
    }

    destroy(list);
*/
    return 0;
}

