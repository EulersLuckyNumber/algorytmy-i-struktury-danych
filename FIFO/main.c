#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FREE(T) do { free (T); T = NULL; } while (0)

typedef struct fifonode node;
typedef struct fifo FIFO;

struct fifonode{
    int value;
    node* next;
};

struct fifo{
    node* head;
    node* tail;
    int size;
};

FIFO* create(){
    FIFO* fifo = (FIFO*) malloc(sizeof(FIFO));
    fifo->head = NULL;
    fifo->tail = NULL;
    fifo->size = 0;
    return fifo;
}

void enqueue(FIFO* fifo, int value){

    //tworzymy nowy element i przypisujemy mu wartość
    node* element = (node*) malloc(sizeof(node));
    element->value = value;
    element->next = NULL;
    //jesli w kolejce nic nie ma ten element będzie szczytem i spodem na raz
    if(fifo->size == 0){
        fifo->head = element;
        fifo->tail = element;
    }
    else{
        fifo->tail->next = element;
        fifo->tail = element;
    }
    ++fifo->size;
    return;
}

int dequeue(FIFO* fifo){
    if(fifo->size > 0){
        node* temp = fifo->head;
        int element = fifo->head->value;
        fifo->head = fifo->head->next;
        FREE(temp);
        --fifo->size;
        return element;
    }
    else{
        printf("\nDequeue error: queue empty\n");
        return -1;
    }
}

int getElementValue(FIFO* fifo, int position){
    if(position < 1){
        printf("\nPosition error: number too small\n");
        return -1;
    }
    if(position > fifo->size){
        printf("\nPosition error: number too big. Size of queue is %d\n", fifo->size);
        return -1;
    }
    if(fifo->size > 0){
        int i;
        node* element = fifo->head;
        for(i = 1; i<position; ++i){
            element = element->next;
        }

        return element->value;
    }
    else{
        printf("\nElement getting error: queue empty\n");
        exit(1);
    }
}

void empty(FIFO* fifo){
    printf("\nmaking queue empty\n\n");
    while(fifo->size > 0){
        node* temp = fifo->head;
        fifo->head = fifo->head->next;
        FREE(temp);
        --fifo->size;
    }
}

void destroy(FIFO* fifo){
    printf("\ndestroying queue\n\n");
    if(fifo->size > 0){
        empty(fifo);
    }
    free(fifo);
}

int main(){

    FIFO* fifo = create();
    enqueue(fifo, 10);
    enqueue(fifo, 20);
    enqueue(fifo, 30);
    enqueue(fifo, 50);

    printf("head: %d\n", fifo->head->value);
    //printf("3rd: %d\n", fifo->head->next->next->value);
    printf("tail: %d\n", fifo->tail->value);

    //destroy(fifo);
    //empty(fifo);

    printf("get element: %d\n", getElementValue(fifo, 0));
    printf("get element: %d\n", getElementValue(fifo, 1));
    printf("get element: %d\n", getElementValue(fifo, 2));
    printf("get element: %d\n", getElementValue(fifo, 3));
    printf("get element: %d\n", getElementValue(fifo, 4));
    printf("get element: %d\n", getElementValue(fifo, 5));


    printf("dequeue: %d\n", dequeue(fifo));
    enqueue(fifo, 50);
    printf("head: %d\n", fifo->head->value);
    //printf("3rd: %d\n", fifo->head->next->next->value);
    printf("tail: %d\n", fifo->tail->value);

    return 0;
}
