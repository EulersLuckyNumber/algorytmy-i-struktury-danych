#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define FREE(T) do { free (T); T = NULL; } while (0)

typedef struct listNode node;
typedef struct list List;

struct listNode{
    int value;
    node* next;
};

struct list{
    node* head;
    node* tail;
    int size;
};

List* create(){
    List* list = (List*) malloc(sizeof(List*));
    list->head = NULL;
    list->tail = NULL;
    list->size = 0;
    return list;
}

void enqueueHead(List* list, int value){
    node* element = (node*) malloc(sizeof(node*));
    element->value = value;

    if(list->size == 0){
        list->head = element;
        list->tail = element;
        element->next = NULL;
    }
    else{
        element->next = list->head;
        list->head = element;
    }

    ++list->size;
}

void enqueueTail(List* list, int value){
    node* element = (node*) malloc(sizeof(node*));
    element->value = value;

    if(list->size == 0){
        list->head = element;
        list->tail = element;
        element->next = NULL;
    }
    else{
        list->tail->next = element;
        list->tail = element;
    }

    ++list->size;
}

int dequeueHead(List* list){
    if(list->size > 1){
        int value = list->head->value;
        node* temp = list->head;

        list->head = temp->next;
        FREE(temp);
        --list->size;
        return value;
    }else if(list->size == 1){
        int value = list->head->value;
        FREE(list->head);
        --list->size;
        return value;
    }
    else{
        printf("\nDequeue error: queue empty\n");
        return -1;
    }
}

int dequeueTail(List* list){
    if(list->size > 1){
        node* temp = list->head;
        int i;
        for(i = 1; i < list->size - 1; ++i){
            temp = temp->next;
        }
        int value = temp->next->value;
        FREE(temp->next);
        list->tail = temp;
        --list->size;
        return value;
    }
    else if(list->size == 1){
        int value = list->head->value;
        FREE(list->head);
        --list->size;
        return value;
    }
    else{
        printf("\nDequeue error: queue empty\n");
        return -1;
    }
}

int getValue(List* list, int position){
    if(position > list->size || position < 1){
        printf("wrong position indicator\n");
        return -1;
    }
    int i;
    node* element = list->head;
    for(i = 1; i < position; i++){
        element = element->next;
    }

    return element->value;
}

void empty(List* list){
    node* element;
    while(list->size > 1){
        element = list->head;
        list->head = list->head->next;
        FREE(element);
        --list->size;
    }
    FREE(element);
    --list->size;
}

void destroy(List* list){
    if(list->size > 0){
        empty(list);
    }
    FREE(list);
    list->head = NULL;
}

int search(List* list, int value){
    int position = 1;
    node* element = list->head;

    while(element != list->tail){
        if(value == element->value){
            printf("searching success. Value %d found on position: %d\n",value, position);
            return position;
        }
        element = element->next;
        ++position;
    }

    if(value == element->value){
        printf("searching success. Value %d found on position: %d\n",value, position);
        return position;
    }
    else{
        printf("Searching failed. Returning value 0\n");
        return 0;
    }


}


void merge(List* list1, List* list2){
    printf("merging\n");
    list1->tail->next = list2->head;
    list1->size += list2->size;
    list1->tail = list2->tail;
    list2->head = NULL;
    list2->tail = NULL;
    FREE(list2);
}

void randomAccess(List* list){
    int i = 0;
    double avg = 0;
    clock_t start_t, end_t;
    for(i = 1; i<=50; i++){
        start_t = clock();
        getValue(list, rand()%1000);
        //printf("value: %d\n", getValue(list, rand()%1000));
        end_t = clock();
        //printf("time: %ld\n", (end_t - start_t));
        avg += (end_t - start_t);
    }
    printf("avg for random: %lf\n", avg/50);
}
void constAccess(List* list){
    int i = 0;
    int j = 800;
    double avg = 0;
    clock_t start_t, end_t;
    for(i = 1; i<50; i++){
    //    j = i*20-1;
        start_t = clock();
        getValue(list, j);
        //printf("value: %d\n", getValue(list, rand()%1000));
        end_t = clock();
        //printf("time: %ld\n", (end_t - start_t));
        avg += (end_t - start_t);
    }
    printf("avg for constant position: %lf\n", avg/50);
}

void accessTest(){
    int i;
    int seed = time(NULL);
    srand(seed);
    List* list = create();
    for(i = 0; i<1000; i++){
        enqueueHead(list, rand());
        //printf("%d\n", list->head->value);
    }

    for(i=0; i<10; ++i){
        randomAccess(list);
    }
    for(i=0; i<10; ++i){
        constAccess(list);
    }

}

int main(void)
{
    accessTest();
    /*
    List* list = create();
    List* list2 = create();
    enqueueHead(list, 10);
    enqueueTail(list, 20);
    enqueueHead(list, 30);
    enqueueHead(list, 50);
    enqueueHead(list, 60);
    enqueueTail(list, 70);
    enqueueTail(list, 80);
    enqueueTail(list, 90);
    enqueueHead(list, 40);

    enqueueHead(list2, 10);
    enqueueTail(list2, 20);
    enqueueHead(list2, 30);
    enqueueHead(list2, 50);

    merge(list, list2);

    int i;
    node* element = list->head;
    for(i = 0; i < list->size; ++i){
        printf("nr %d = %d \n", i+1, element->value);
        element = element->next;
    }

    printf("search: %d\n", search(list, 20));

    printf("dequeueTail: %d\n", dequeueTail(list));
    printf("dequeueHead: %d\n", dequeueHead(list));
    printf("size: %d\n", list->size);

    //destroy(list);


    element = list->head;
    for(i = 0; i < list->size; ++i){
        printf("nr %d = %d \n", i+1, element->value);
        element = element->next;
    }
/*
    element = list->head;
    for(i = 0; i < list->size; ++i){
        printf("get value nr %d = %d \n", i+1, getValue(list, i+1));
        element = element->next;
    }

    /*
    clock_t start_t, end_t;
    start_t = clock();
    int i = 0;
    for(i = 0; i<20; i++){
        start_t = clock();
        greatTest(list);
        end_t = clock();
        printf("%ld\n", end_t - start_t);
    }

    destroy(list);
*/

    return 0;
}
